filterSelection("all");
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("filterDiv");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
}

function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
}

// Add active class to the current button (highlight it)
var btnContainer = document.getElementById("myBtnContainer");
var btns = btnContainer.getElementsByClassName("menu-btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function () {
    var current = document.getElementsByClassName("menu-btn active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}
$("#gallery").owlCarousel({
  loop: true,
  margin: 1,
  nav: false,
  autoplay: true,
  dots: true,
  dotsEach: 2,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 3,
    },
  },
});
$("#client").owlCarousel({
  loop: true,
  nav: false,
  autoplay: true,
  dots: true,
  dotsEach: 1,
  items: 1,
});

$(".navbar-nav .nav-link").on("click", function () {
  $(".navbar-nav").find(".active").removeClass("active");
  $(this).addClass("active");
});

//back to top
const showOnPx = 200;
const backToTopButton = document.querySelector(".btn-fixed");

const scrollContainer = () => {
  return document.documentElement || document.body;
};
document.addEventListener("scroll", () => {
  if (scrollContainer().scrollTop > showOnPx) {
    backToTopButton.classList.remove("hidden");
  } else {
    backToTopButton.classList.add("hidden");
  }
});
